#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileSystemModel>
#include <QTreeView>
#include "loopthread.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

public slots:
    void setMaxCountFiles(int k);
    void setCurrentCountFiles(int k);

private slots:
    void on_pbt_browseFirstDir_clicked();
    void on_pbt_browseFirstDir_2_clicked();
    void on_pbt_Check_clicked();

private:
    Ui::MainWindow *ui;

    loopThread          *newThread;
    QStringList         m_ResultList;
    QStringList         m_FirstDirFilesPath;
    QStringList         m_SecondDirFilesPath;
    QString             m_FirstDirPath;
    QString             m_SecondDirPath;

    void showNoSameFileInListView();
    void showSameFileInListView(QStringList fileList);
    void showErrorScreen(QString errorText);
    void setFirstDirPath(QString path);
    void setSecondDirPath(QString path);

private slots:
    void showAllData();

};

#endif // MAINWINDOW_H
