#include "loopthread.h"

loopThread::loopThread()
{

}

void loopThread::setFirstPath(QString *path)
{
    m_FirstDirPath = path;
}

void loopThread::setSecondPath(QString *path)
{
    m_SecondDirPath = path;
}


void loopThread::run()
{

    m_FirstDirFilesPath = utilits::getFileList(*m_FirstDirPath);
    m_SecondDirFilesPath = utilits::getFileList(*m_SecondDirPath);

    if(!m_FirstDirFilesPath.isEmpty() && !m_SecondDirFilesPath.isEmpty())
    {
        QByteArray firstMD5, secondMD5;
        QString nameFirst, nameSecond;
        emit setMaxCountFiles(m_FirstDirFilesPath.size() + m_SecondDirFilesPath.size());

        QMultiMap<QString, QString> firstDir;
        for(int firstIter = 0; firstIter < m_FirstDirFilesPath.size(); firstIter++)
        {
            firstDir.insert(utilits::getNameFile(m_SecondDirFilesPath.at(firstIter)), m_FirstDirFilesPath.at(firstIter));
            emit setCurrentCountFiles(firstIter);
        }

        for(int secondIter = 0; secondIter < m_SecondDirFilesPath.size(); secondIter++)
        {
            nameSecond= utilits::getNameFile(m_SecondDirFilesPath.at(secondIter));
            if(firstDir.contains(nameSecond))
            {
                firstMD5 = utilits::getFileChecksum(firstDir.take(nameSecond));
                secondMD5 = utilits::getFileChecksum(m_SecondDirFilesPath.at(secondIter));
                if (firstMD5 == secondMD5 )
                    m_ResultList.append(m_FirstDirFilesPath.at(secondIter));
            }
            emit setCurrentCountFiles(m_FirstDirFilesPath.size() + secondIter);
        }

    }

}

