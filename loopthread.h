#ifndef LOOPTREAD_H
#define LOOPTREAD_H

#include <QThread>
#include <QtCore>
#include "utilits.h"

class loopThread : public QThread
{
    Q_OBJECT
public:
    loopThread();


    void setFirstPath(QString *path);
    void setSecondPath(QString *path);
    QStringList getResultList() {return m_ResultList;}

    QStringList         m_ResultList;
protected:
    void run();


private:
    QStringList         m_FirstDirFilesPath;
    QStringList         m_SecondDirFilesPath;
    QString             *m_FirstDirPath;
    QString             *m_SecondDirPath;

signals:
    void                setMaxCountFiles(int);
    void                setCurrentCountFiles(int);
};

#endif // LOOPTREAD_H
