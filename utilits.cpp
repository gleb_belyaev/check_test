#include "utilits.h"

utilits::utilits()
{

}

QString utilits::getNameFile(QString fullPathName)
{
    QStringList namePartList = fullPathName.split("/");
    QString name = namePartList.last();
    return name;

}

QByteArray utilits::getFileChecksum(QString fileName)
{
    QFile f(fileName);
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(QCryptographicHash::Md5);
        if (hash.addData(&f)) {
            return hash.result();
        }
    }
    return QByteArray();
}

QStringList utilits::getFileList(QString dirName)
{
    QStringList resultList;
    QDir dir(dirName);
    QFileInfoList fileInfoList = dir.entryInfoList();
    if(fileInfoList.size() > 2)
    {
        QList<QFileInfo>::iterator iter = fileInfoList.begin();
        QString path;
        for(iter=fileInfoList.begin() + 2; iter != fileInfoList.end(); iter++)
        {
            path = iter->absoluteFilePath();
            if(iter->isDir())
                resultList += getFileList(path);
            else
                resultList.append(path);
        }
    }
    return resultList;
}
