
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QFile>
#include <QDebug>
#include <QFileSystemModel>
#include "utilits.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->progressBar->setValue(0);


}

MainWindow::~MainWindow()
{
    delete ui;
}



//*****************************FIRST DIRECTORY**********************************

void MainWindow::on_pbt_browseFirstDir_clicked()
{
    setFirstDirPath(QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/Users/Gleb/Documents", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));

}

void MainWindow::setFirstDirPath(QString path)
{
    ui->le_FirstDirPath->setText(path);
}

//*****************************SECOND DIRECTORY**********************************

void MainWindow::on_pbt_browseFirstDir_2_clicked()
{
    setSecondDirPath(QFileDialog::getExistingDirectory(this, tr("Open Directory"), "/Users/Gleb/Documents", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks));
}

void MainWindow::setSecondDirPath(QString path)
{
    ui->le_SecondDirPath->setText(path);
}



//*****************************CHANGE DIRECTORY**********************************

void MainWindow::on_pbt_Check_clicked()
{
    m_ResultList.clear();

    ui->progressBar->setValue(0);

    m_FirstDirPath = ui->le_FirstDirPath->text();
    m_SecondDirPath = ui->le_SecondDirPath->text();

    if(!m_FirstDirPath.isEmpty() && !m_SecondDirPath.isEmpty())
    {
        ui->pbt_Check->setStyleSheet("background-color:#3f51b5;");
        ui->pbt_Check->setEnabled(false);
        newThread = new loopThread();
        newThread->setFirstPath(new QString(m_FirstDirPath));
        newThread->setSecondPath(new QString(m_SecondDirPath));
        connect(newThread, SIGNAL(finished()), this, SLOT(showAllData()));
        connect(newThread, SIGNAL(setMaxCountFiles(int)), this, SLOT(setMaxCountFiles(int)), Qt::UniqueConnection);
        connect(newThread, SIGNAL(setCurrentCountFiles(int)), this, SLOT(setCurrentCountFiles(int)), Qt::UniqueConnection);
        newThread->start();
    } else
        showErrorScreen("Выбранная вами папка пуста!");
}


//*****************************SHOW ERROR**********************************
void MainWindow::showErrorScreen(QString errorText)
{
    if(m_FirstDirPath.isEmpty())
        ui->le_FirstDirPath->setStyleSheet("background-color:white; border:3px solid #c62828;");

    if(m_SecondDirPath.isEmpty())
        ui->le_SecondDirPath->setStyleSheet("background-color:white; border:3px solid #c62828;");

    if (m_FirstDirFilesPath.isEmpty() && !m_FirstDirPath.isEmpty())
        ui->le_FirstDirPath->setStyleSheet("background-color:white; border:3px solid #c62828;");

    if(m_SecondDirFilesPath.isEmpty() && !m_SecondDirPath.isEmpty())
        ui->le_SecondDirPath->setStyleSheet("background-color:white; border:3px solid #c62828;");


    ui->lbl_countSame->setStyleSheet("color:#c62828");
    ui->lbl_countSame->setText(errorText);
    showNoSameFileInListView();
}


//*****************************UPDATE TABLEVIEW**********************************


void MainWindow::showAllData()
{
    ui->le_FirstDirPath->setStyleSheet("background-color:white; border:none;");
    ui->le_SecondDirPath->setStyleSheet("background-color:white; border:none;");
    ui->lbl_countSame->setStyleSheet("color:white;");

    ui->progressBar->setValue(ui->progressBar->maximum());
    ui->pbt_Check->setEnabled(true);
    ui->pbt_Check->setStyleSheet("QPushButton { border:none; background-color:#536DFE; color:white; } QPushButton:hover { background-color:#3f51b5;}");

    m_ResultList = newThread->getResultList();

    if(m_ResultList.size() != 0)
    {
        showSameFileInListView(m_ResultList);
        ui->lbl_countSame->setText(QString::number(m_ResultList.size()) + " шт.");
    }
    else
    {
        ui->lbl_countSame->setText("0 шт.");
        showNoSameFileInListView();
    }

    showSameFileInListView(m_ResultList);
}

void MainWindow::showNoSameFileInListView()
{
    QStringListModel *model = new QStringListModel(this);
    ui->lv_sameFilesList->setModel(model);
}

void MainWindow::showSameFileInListView(QStringList fileList)
{
    QStringListModel *model = new QStringListModel(this);
    model->insertRow(fileList.count());
    model->setStringList(fileList);
    ui->lv_sameFilesList->setModel(model);
}

//*****************************UPDATE PROGRESSBAR**********************************

void MainWindow::setMaxCountFiles(int k)
{
    qDebug() << k;
    ui->progressBar->setMaximum(k);
}

void MainWindow::setCurrentCountFiles(int k)
{
    ui->progressBar->setValue(k);
}
