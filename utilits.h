#ifndef UTILITS_H
#define UTILITS_H

#include <QtCore>


class utilits
{
public:
    utilits();
    static QString getNameFile(QString dir_name);
    static QStringList getFileList(QString dir_name);
    static QByteArray getFileChecksum(QString fileName);
};

#endif // UTILITS_H
