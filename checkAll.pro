#-------------------------------------------------
#
# Project created by QtCreator 2016-04-11T19:10:00
#
#-------------------------------------------------

QT       += core gui
QT       += widgets

greaterThan(QT_MAJOR_VERSION, 4):

TARGET = checkAll
TEMPLATE = app


SOURCES +=  main.cpp\
            mainwindow.cpp \
            utilits.cpp \
            loopthread.cpp

HEADERS  += mainwindow.h \
            utilits.h \
            loopthread.h

FORMS    += mainwindow.ui
